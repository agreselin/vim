set modelines=0       " How many lines vim checks for modelines. Modelines are insecure, https://security.stackexchange.com/questions/36001/vim-modeline-vulnerabilities
set title             " Make the terminal inherit the title from Vim. http://vim.wikia.com/wiki/Vim_xterm_titlem
set hidden            " Don't prompt for saving before switching buffers.
set nowrap            " Don't wrap when viewing.
set textwidth=0       " Don't wrap while writing. https://vim.fandom.com/wiki/Automatic_word_wrapping
set linebreak         " With line wrap on, wrap between words instead of at the last character that fits on the screen.
set nostartofline     " Stay on the current column when moving with G and the like (see :help startofline).
set ignorecase
set smartcase         " Search case insensitively unless the sarch pattern contains upper case characters.
set foldopen-=search  " Search only in unfolded lines. https://vim.fandom.com/wiki/Search_only_in_unfolded_text
set virtualedit=block " Make up whitespace for visual block mode.
set sidescroll=1
set scrolloff=1
set history=10000     " Set the number of ':' commands and search patterns to be kept in history. 10.000 is the highest possible value. See ':help 'history'' and https://vi.stackexchange.com/questions/5880/how-to-have-endless-vim-history

" Copy and paste between different vim instances without overwriting the system clipboard.
" Set only for terminal sessions, to avoid interfering with gvim's settings in gvimrc.
" Requires clipboard support. Check that :echo has('clipboard') outputs 1. If not, consider installing vim-gtk3.
" https://vi.stackexchange.com/questions/4600/how-to-copy-across-terminals
" https://stackoverflow.com/questions/3961859/how-to-copy-to-clipboard-in-vim#comment67886976_3961859
if !has('gui_running')
    set clipboard^=unnamed
endif

" Reduce the ESC key's delay
" https://www.johnhawthorn.com/2012/09/vi-escape-delays/
set timeoutlen=1000 ttimeoutlen=10

" Use UTF-8
" https://unix.stackexchange.com/questions/23389/how-can-i-set-vims-default-encoding-to-utf-8
" http://vim.wikia.com/wiki/Working_with_Unicode
" https://stackoverflow.com/questions/12814371/conversion-error-in-vim
scriptencoding utf-8
if has("multi_byte")
    if &termencoding == ""
        let &termencoding = &encoding
    endif
    set encoding=utf-8
    setglobal fileencoding=utf-8
    "setglobal bomb
    set fileencodings=ucs-bom,utf-8,latin1
endif

" Use gnuplot syntax highlighting with .gp files
" By default, Vim uses it with .gpi files. https://groups.google.com/forum/#!topic/comp.editors/Sb4nPiSntZ0
" http://vim.wikia.com/wiki/Forcing_Syntax_Coloring_for_files_with_odd_extensions
" https://stackoverflow.com/questions/11666170/vim-persistent-set-syntax-for-a-given-filetype/28117335#28117335
if has("autocmd")
    augroup filetype_gnuplot
        autocmd!
        autocmd BufNewFile,BufRead *.gp set filetype=gnuplot
    augroup END
endif

" Disable placing of the cursor on its last position when a file is opened
" That's due to an autocommand in Fedora's /etc/vimrc.
" https://unix.stackexchange.com/questions/59486/disable-vi-from-going-to-the-last-visited-line-upon-file-opening
:au! fedora BufReadPost

" Disable automatic insertion of comment characters
" That behaviour is because of the C file plugin in Vim. Since the file plugin is loaded after loading .vimrc, the settings in .vimrc are overwritten.
" Since the 'autocmd' is wrapped in an 'augroup' statement, resourcing .vimrc won't re-register it.
" https://stackoverflow.com/questions/16030639/vim-formatoptions-or/23326474#23326474
" See also https://vim.fandom.com/wiki/Disable_automatic_comment_insertion
"          https://superuser.com/questions/271023/can-i-disable-continuation-of-comments-to-the-next-line-in-vim
"          https://stackoverflow.com/questions/6076592/vim-set-formatoptions-being-lost
augroup Format-Options
    autocmd!
    autocmd BufEnter * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
    " This can be done as well instead of the previous line, for setting formatoptions as you choose:
    "autocmd BufEnter * setlocal formatoptions=crqn2l1j
augroup END

" Switch buffer without changing the cursor's position within the window
" http://vim.wikia.com/wiki/Avoid_scrolling_when_switch_buffers
" Save current view settings on a per-window, per-buffer basis.
function! AutoSaveWinView()
    if !exists("w:SavedBufView")
        let w:SavedBufView = {}
    endif
    let w:SavedBufView[bufnr("%")] = winsaveview()
endfunction
" Restore current view settings.
function! AutoRestoreWinView()
    let buf = bufnr("%")
    if exists("w:SavedBufView") && has_key(w:SavedBufView, buf)
        let v = winsaveview()
        let atStartOfFile = v.lnum == 1 && v.col == 0
        if atStartOfFile && !&diff
            call winrestview(w:SavedBufView[buf])
        endif
        unlet w:SavedBufView[buf]
    endif
endfunction
" When switching buffers, preserve window view.
if v:version >= 700
    augroup switch_buf_preserve_view
        autocmd!
        autocmd BufLeave * call AutoSaveWinView()
        autocmd BufEnter * call AutoRestoreWinView()
    augroup END
endif

" Like windo except this ensures that the cursor is not moved to another window
" https://vim.fandom.com/wiki/Run_a_command_in_multiple_buffers
function! WinDo(command)
  let currwin=winnr()
  let curaltwin=winnr('#')
  execute 'windo ' . a:command
  " restore previous/alt window
  execute curaltwin . 'wincmd w'
  " restore current window
  execute currwin . 'wincmd w'
endfunction
com! -nargs=+ -complete=command WinDo call WinDo(<q-args>)


"" Key bindings

" Quicker shortcuts for switching buffer
nnoremap <silent> <C-x><Right> :bn<CR>
nnoremap <silent> <C-x><Left> :bp<CR>

" Clear search highlights on pressing C-l
" https://vi.stackexchange.com/questions/184/how-can-i-clear-word-highlighting-in-the-current-document-e-g-such-as-after-se
nnoremap <silent> <C-L> :nohlsearch<CR><C-L>

" Delete word ahead in insert mode
" https://stackoverflow.com/questions/32154641/vim-how-to-delete-a-word-in-the-insert-mode
inoremap <C-a> <C-o>dw

" Find within a visual selection
" http://vim.wikia.com/wiki/Search_and_replace_in_a_visual_selection
vnoremap <M-/> <Esc>/\%V
nnoremap <M-/> /\%V

" Jump to changes in diff mode with a single key stroke
" Adapted from https://vi.stackexchange.com/questions/2705/create-mappings-that-only-apply-to-diff-mode
nnoremap <expr> <C-j> &diff ? '[c' : null
nnoremap <expr> <C-k> &diff ? ']c' : null

" Unbind 'diffput' from 'dp'
" https://vi.stackexchange.com/questions/38559/remap-diffget-and-disable-the-binding-to-do
nnoremap dp <nop>

" Jump over blocks of text or whitespace along the current column (sort of vertical 'B', 'E' and 'W')
" It requires the vim-columnmove plugin, https://github.com/machakann/vim-columnmove
" See also https://vi.stackexchange.com/questions/15151/move-to-the-first-last-non-whitespace-character-of-the-column-vertical-w-b-e
" TODO: if the cursor is on the lowermost “anchor point”, move to the last line on running 'columnmove-W'.
map <C-Up> <Plug>(columnmove-B)
map <C-Down> <Plug>(columnmove-W)
map <C-S-Down> <Plug>(columnmove-E)

" Type 'gb' in command mode to get the list of open buffers and vim ready to take a buffer name/number to switch to.
" https://stackoverflow.com/questions/53664/how-to-effectively-work-with-multiple-files-in-vim#comment45822027_53714
nnoremap gb :ls<cr>:b<space>

" Type :CDC to cd to the current file's directory
" http://vim.wikia.com/wiki/Set_working_directory_to_the_current_file
command CDC lcd %:p:h

" Search in all currently opened buffers
" Start a search with ':SearchBuffers <pattern>'; jump to the next result with :cn and to the previous with :cp.
" The search pattern has the syntax shown at :help :vimgrep
" http://vim.wikia.com/wiki/Search_on_all_opened_buffers
" with a bit ('| copen') of https://stackoverflow.com/questions/11975174/how-do-i-search-the-open-buffers-in-vim
function! ClearQuickfixList()
    call setqflist([])
endfunction
function! Vimgrepall(pattern)
    call ClearQuickfixList()
    exe 'bufdo vimgrepadd ' . a:pattern . ' % | copen'
    cnext
endfunction
command! -nargs=1 SearchBuffers call Vimgrepall(<f-args>)

" Press F2 to remove all trailing whitespace
" http://vim.wikia.com/wiki/Remove_unwanted_spaces
" https://vi.stackexchange.com/questions/454/whats-the-simplest-way-to-strip-trailing-whitespace-from-all-lines-in-a-file
nnoremap <silent> <F2> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>

" Toggle line wrapping in all windows in the current tab
" REVIEW: make it display the new value, without prompting to hit ENTER to continue.
map <silent> <F5> :WinDo setlocal wrap! <CR>
map <silent> <C-p> :WinDo setlocal wrap! <CR>

" Toggle the current line highlighter with F6
nnoremap <silent> <F6> :setlocal cursorline!<CR>
imap <silent> <F6> <C-O><F6>

" Toggle hlsearch with F7
nnoremap <F7> :setlocal hlsearch!<CR>

" Toggle 'code mode' for the Tab key with F8
" https://www.cs.swarthmore.edu/help/vim/indenting.html
" http://vim.wikia.com/wiki/Indenting_source_code
" http://vim.wikia.com/wiki/Toggle_between_tabs_and_spaces
" http://vim.wikia.com/wiki/Quick_generic_option_toggling
" http://learnvimscriptthehardway.stevelosh.com/chapters/38.html#toggling-options
function! TabToggle()
    if &smartindent
        setlocal shiftwidth&
        setlocal tabstop&
        setlocal noexpandtab
        setlocal noautoindent
        setlocal nosmartindent
        setlocal smartindent?
    else
        setlocal shiftwidth=4 " Indent by 4 spaces
        setlocal tabstop=4    " Tab inserts 4 spaces; see also the softtabstop option, https://vi.stackexchange.com/questions/4244/what-is-softtabstop-used-for
        setlocal expandtab    " Use spaces instead of tabs
        setlocal autoindent
        setlocal smartindent  " smartindent requires autoindent; about that and the difference between the two, see https://vi.stackexchange.com/questions/5818/what-is-the-difference-between-autoindent-and-smartindent-in-vimrc
        setlocal smartindent?
    endif
endfunction
nnoremap <F8> :call TabToggle()<CR>
imap <F8> <C-O><F8>

" Toggle the undo-tree panel with F9
" It uses the undotree plugin, https://github.com/mbbill/undotree
nnoremap <F9> :UndotreeToggle<CR>


"" Look

" For information about colour settings, see https://alvinalexander.com/linux/vi-vim-editor-color-scheme-syntax

set termguicolors " Enable 24 bit colour

" Switch on syntax highlighting
syntax on
" Disable syntax highlighting in diff mode
if &diff
    syntax off
endif

function SetColours()
    if &background == 'dark'
        " Style for the current line highlighter
        " Use the colour of Fedora 29 Gnome Terminal's menu bar.
        " http://vim.wikia.com/wiki/Highlight_current_line
        highlight CursorLine cterm=none guibg=#33393B
        " Style for visual selections
        " Use a variation generated by paletton.com of #232729, the background colour in Fedora 29's Gnome Terminal.
        " https://stackoverflow.com/questions/3074068/how-to-change-the-color-of-the-selected-code-vim-scheme
        highlight Visual cterm=none guibg=#42494D
    elseif &background == 'light'
        " Style for the current line highlighter
        " Use a variation on the hue of the accent colour (#3584E4) in Gnome Terminal 3.32.2.
        highlight CursorLine cterm=none guibg=#E0EEFF
        " Style for visual selections
        " Use a variation on the hue of the accent colour (#3584E4) in Gnome Terminal 3.32.2.
        highlight Visual cterm=none guibg=#D6E9FF
    endif
endfunction

" Apply SetColours() only in terminal sessions
if !has('gui_running')
    set background=light
    call SetColours()
    autocmd OptionSet background call SetColours() " Call 'SetColours()' when the variable 'background' is set.
    " Colour of the tildes that indicate non-existing lines at the end of file
    " Use a variation generated by paletton.com of #215D9C, the colour used to underline the current tab in Fedora 29's Gnome Terminal.
    " https://stackoverflow.com/questions/3725526/how-do-i-change-the-character-vim-uses-to-number-blank-lines-in-buffer
    highlight NonText guifg=#618EBE
endif

" Highlight the line the cursor is on, unless vim is in diff mode
" Note: cursorline overrides other text background settings.
if !&diff
    set cursorline
endif

" Set the cursor's shape
" The '&term' check's main purpose is to avoid applying these settings when Vim runs on a TTY console, where they don't work.
" http://vim.wikia.com/wiki/Change_cursor_shape_in_different_modes
if &term=~'xterm-256color'
    " Use these settings on GNOME Terminal
    " On Konsole you have to wait until the next cursor blink for it to change shape, with these settings.
    let &t_EI = "\<Esc>[1 q" " Normal mode: blinking block
    let &t_SI = "\<Esc>[5 q" " Insert mode: blinking bar
    let &t_SR = "\<Esc>[3 q" " Replace mode: blinking underline
    " Use these settings on Konsole
    " Taken from https://github.com/jszakmeister/vim-togglecursor/blob/master/plugin/togglecursor.vim
    "let &t_EI = "\<Esc>]50;CursorShape=0;BlinkingCursorEnabled=1\x7" " Normal mode: blinking block
    "let &t_SI = "\<Esc>]50;CursorShape=1;BlinkingCursorEnabled=1\x7" " Insert mode: blinking bar
    "let &t_SR = "\<Esc>]50;CursorShape=2;BlinkingCursorEnabled=0\x7" " Replace mode: blinking underline
endif


"" Backup and auxiliary files

" The double tailing slash will store files using full paths in their names.
" https://news.ycombinator.com/item?id=1688068
" ! It doesn't work; see https://stackoverflow.com/questions/26742313/why-vim-backup-filenames-are-not-correct-backupdir-option-not-performing-as-e
"                        https://github.com/vim/vim/issues/179

" Make backups
set backup
" Put them in the following directory
" http://vimdoc.sourceforge.net/htmldoc/options.html#'backupdir'
set backupdir=~/.backups//
" Make the backup directory if it doesn't exist
" https://stackoverflow.com/questions/5700389/using-vims-persistent-undo#comment45453522_22676189
silent call system('mkdir -p ' . &backupdir)

" Persistent undo
set undofile
set undodir=~/.vim/undo//
" Make the undo directory if it doesn't exist
silent call system('mkdir -p ' . &undodir)

" https://stackoverflow.com/questions/6286866/how-to-tell-vim-to-store-the-viminfo-file-somewhere-else
" https://stackoverflow.com/questions/23012391/how-and-where-is-my-viminfo-option-set
set viminfo+=n~/.vim/viminfo

" Fortran
" See ':help fortran'
let fortran_free_source=1
let fortran_more_precise=1

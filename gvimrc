" 1. share the system clipboard and
" 2. store the last yank in the * register. This way "*p pastes only text that has been yanked (not that which has been deleted, unless it's been deleted in visual mode), even on different files.
set clipboard^=unnamed,unnamedplus

" Automatically cd to the current file's directory
set autochdir

" Window size
" http://vim.wikia.com/wiki/Maximize_or_set_initial_window_size
set columns=100

" Hide the menu bar and the toolbar
" http://vim.wikia.com/wiki/Hide_toolbar_or_menus_to_see_more_text
set guioptions-=m  "Remove menu bar. Type :set guioptions+=m to re-enable it.
set guioptions-=T  "remove toolbar
" Toggle the menu bar with F11
nnoremap <F11> :if &go=~#'m'<Bar>set go-=m<Bar>else<Bar>set go+=m<Bar>endif<CR>
imap <F11> <C-O><F11>

" Set the default typeface
" https://codeyarns.com/2015/03/11/how-to-set-font-in-gvim/
set guifont=Droid\ Sans\ Mono\ 10"

" Style for the current line highlighter
highlight CursorLine cterm=none guibg=lightgreen
